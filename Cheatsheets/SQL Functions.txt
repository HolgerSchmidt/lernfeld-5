Important SQL Functions:

1. SELECT statement
	The SELECT function is used to select data fromn the database.
	Example syntax: SELECT * FROM rezepte
	With this example you would select everything that is in the table "rezepte".
	The "*" is for everything.
	
2. WHERE statemant
	With the WHERE statement you can set a condition for the data you select.
	Example syntax: SELECT * FROM rezepte WHERE kalorien > 200
	With this example you would select every record from the table where the condition kalorien > 200 is true.
	There are also statement like AND, OR and NOT operator so you can set multiple conditions.
	
- NULL operator:
	With the NULL operator you can select fields with no value.
	For example: SELECT * FROM rezepte WHERE kalorien IS NULL
	In this example you would select every record where the column kalorien has no value.
	You can also use IS NOT NULL to select everything that has a value.
	
- LIKE operator:
	If you want to select a string you need to use the LIKE statement and put the string in "".
	For example: SELECT * FROM zutat WHERE einheit LIKE "Stück" AND kalorien > 200
	This would select every record if there is Stück in the column einheit and kalorien above 200.
	
- wildcard operator:
	For Strings you can also use wildcard character like:
	%	Represents zero or more characters					[bl% finds bl, black, blue, and blob]
	_	Represents a single character						[h_t finds hot, hat, and hit]
	[]	Represents any single character within the brackets	[h[oa]t finds hot and hat, but not hit]
	^	Represents any character not in the brackets		[h[^oa]t finds hit, but not hot and hat]
	-	Represents a range of characters					[c[a-b]t finds cat and cbt]
	Wildcard operators can also be used in combination.
	
- IN operator:
	The IN operator is a short form for multiple OR conditions.
	Example: SELECT * FROM zutat WHERE einheit IN ('Stück', 'Liter')
	This would select every record with einheit 'Stück' or 'Liter'.
	
- BETWEEN operator:
	The BETWEEn operator select everything within a range.
	For example: SELECT * FROM zutat WHERE kalorien BETWEEN 20 AND 40
	This would select everything where the record kalorien is between 20 and 40.
		
3. AS statement(Aliases):
	With the AS statement you can create a temporary table with a temporary name within the query.
	For example: SELECT kalorien AS kalorien2 FROM zutat
	Im this example you would clone the column kalorien and put it the temporary table kalorien2.
	
4. MIN(), MAX() functions:
	The MIN() function returns the smallest value of the selected column.
	The MAX() function returns the largest value of the selected column.
	For example: SELECT MAX(kalorien) FROM zutat
	This would return the largest record in the column kalorien.
	
5. COUNT(), AVG() and SUM() function:
	The COUNT() function returns the number of rows that matches a specified criterion.
	The AVG() function returns the average value of a numeric column.
	The SUM() function returns the total sum of a numeric column.
	For example: SELECT COUNT(*) FROM zutat
	This example would return the number of rows in the table zutat.
	
6. ORDER BY statement:
	The ORDER BY statement is used to sort the result in ascending or descending order.
	Example: SELECT column1, column2 FROM table1 WHERE column2 IS NOT NULL ORDER BY column1
	In this example you select column1 and column2 from table1 where column2 has a value and the result sorted 
	in ascending order by column1.

7. INSERT INTO:
	It is possible to write the INSERT INTO statement in two ways.
	The first way specifies both the column names and the values to be inserted:
	INSERT INTO table_name (column1, column2, column3 ...) VALUES (value1, value2, value3 ...)
	
	If you are adding values for all the columns of the table, you do not need to specify the column names in the SQL query.
	However, make sure the order of the values is in the same order as the columns in the table.
	The INSERT INTO syntax would be as follows:
	INSERT INTO table_name VALUES (value1, value2, value3 ...)
	
8. UPDATE:
	The UPDATE statement is used to modify the existing records in a table.
	For example: UPDATE table SET column1 = value1, column2 = value2 ... WHERE condition
	!Be carefull with this statement!
	
9. DELETE:
	The DELETE statement is used to delete existing records in a table.
	For example: DELETE FROM table WHERE condition
	!Make sure you use a condition, else you would delete all records!

10. JOINs:
	With JOIN you can combine 2 or more tables. There are different kind of JOIN functions.
	example: SELECT column FROM table1 JOIN table2 ON table1.column = table2.column
	In this example we merge table1 with table2, the is used so the programm knows where to connect the tables.

- INNER JOIN
	The INNER JOIN keyword selects records that have matching values in both tables.

- LEFT JOIN
	The LEFT JOIN keyword returns all records from the left table (table1), and the matched records from the right table (table2).
	The result is NULL from the right side, if there is no match.

- RIGHT JOIN
	The RIGHT JOIN keyword returns all records from the right table (table2), and the matched records from the left table (table1).
	The result is NULL from the left side, when there is no match.

- FULL JOIN
	The FULL OUTER JOIN keyword returns all records when there is a match in left (table1) or right (table2) table records.
	
- self JOIN
	A self JOIN is a regular join, but the table is joined with itself.
	This is used with the AS statement.
	
11. UNION:
	The UNION operator is used to combine the result-set of two or more SELECT statements.
	This conditions must be given:
	- Each SELECT statement within UNION must have the same number of columns
	- The columns must also have similar data types
	- The columns in each SELECT statement must also be in the same order
	Example: SELECT column FROM table1 UNION SELECT column FROM table2
	
12. GROUP BY:
	The GROUP BY statement groups rows that have the same values into summary rows.
	This is used with the aggregate functions [SUM(), AVG(), MIN(), MAX(), COUNT()]

13. HAVING:
	The HAVING clause was added to SQL because the WHERE keyword could not be used with aggregate functions.
	Syntax:
	SELECT column
	FROM table
	WHERE condition
	GROUP BY column
	HAVING condition
	ORDER BY column
	
14. EXISTS:
	The EXISTS operator is used to test for the existence of any record in a subquery.
	The EXISTS operator returns true if the subquery returns one or more records.
	Syntax:
	SELECT column
	FROM table
	WHERE EXISTS
	(SELECT column FROM table WHERE condition)
	
15. ANY and ALL
	The ANY and ALL operators are used with a WHERE or HAVING clause.
	The ANY operator returns true if any of the subquery values meet the condition.
	The ALL operator returns true if all of the subquery values meet the condition.
	Syntax:
	SELECT column
	FROM table
	WHERE column operator ANY
	(SELECT column FROM table WHERE condition)
	
16. SELECT INTO
	The SELECT INTO statement copies data from one table into a new table.
	
	Copy all columns into a new table:
	SELECT *
	INTO newtable
	FROM oldtable
	WHERE condition
	
	Copy only some columns into a new table:
	SELECT column1, column2, column3, ...
	INTO newtable
	FROM oldtable
	WHERE condition
	
17. INSERT INTO SELECT
	The INSERT INTO SELECT statement copies data from one table and inserts it into another table.
	INSERT INTO SELECT requires that data types in source and target tables match.
	The existing records in the target table are unaffected.
	
	Copy all columns example:
	INSERT INTO table2
	SELECT * FROM table1
	WHERE condition
	
	Copy some columns example:
	INSERT INTO table2 (column1, column2, column3 ...)
	SELECT column1, column2, column3 ...
	FROM table1
	WHERE condition
	
18. CASE
	The CASE statement goes through conditions and returns a value when the first condition is met (like an IF-THEN-ELSE statement).
	So, once a condition is true, it will stop reading and return the result.
	If no conditions are true, it returns the value in the ELSE clause.
	If there is no ELSE part and no conditions are true, it returns NULL.
	Example:
	CASE
		WHEN condition1 THEN result1
		WHEN condition2 THEN result2
		WHEN condition3 THEN result3
		ELSE result
	END
	
19. ISNULL()
	Lets you return an alternative value when an expression is NULL:
	SELECT column1, column2 * (column3 + ISNULL(column3, 0))
	
	