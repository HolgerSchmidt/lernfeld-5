import { WebApi } from './WebApi';
import { AxiosResponse } from 'axios';
import { RezeptModel } from '../models/RezeptModel';

export class RezeptService extends WebApi {
  async getAllRecipes(): Promise<AxiosResponse<RezeptModel[]>> {
    return this.request({
      method: 'get',
      url: '/rezept',
    });
  }

  async getRecipeById(id: number): Promise<AxiosResponse<RezeptModel>>{
    return this.request({
      method: 'get',
      url:`/rezept/${id}`
    })
  }

  async createNewRecipe(newRecipe: any): Promise<void> {
    await this.request({
      method: 'post',
      url: '/rezept',
      data: newRecipe,
    });
  }
}

const rezeptService = new RezeptService();
export default rezeptService;
