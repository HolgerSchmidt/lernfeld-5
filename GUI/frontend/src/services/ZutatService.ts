import { WebApi } from './WebApi';
import { AxiosResponse } from 'axios';
import { ZutatModel } from '../models/ZutatModel';
import { ZutatDto } from '../dtos/ZutatDto';

export class ZutatService extends WebApi {
  async getAllIngredients(): Promise<AxiosResponse<ZutatModel[]>> {
    return this.request({
      method: 'get',
      url: '/zutat',
    });
  }

  async createNewIngredient(
    ingredient: ZutatDto
  ): Promise<AxiosResponse<ZutatDto>> {
    return this.request({
      method: 'post',
      url: '/zutat',
      data: ingredient,
    });
  }
}

const zutatService = new ZutatService();
export default zutatService;
