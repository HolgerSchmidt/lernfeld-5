import { WebApi } from './WebApi';
import { AxiosResponse } from 'axios';
import { AllergieModel } from '../models/AllergieModel';

export class AllergieService extends WebApi {
  async getAllAllergies(): Promise<AxiosResponse<AllergieModel[]>> {
    return this.request({
      method: 'get',
      url: '/allergie',
    });
  }
}

const allergieService = new AllergieService();
export default allergieService;
