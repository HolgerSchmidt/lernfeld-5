import { WebApi } from './WebApi';
import { AxiosResponse } from 'axios';
import { LieferantModel } from '../models/LieferantModel';

export class LieferantService extends WebApi {
  async getAllSuppliers(): Promise<AxiosResponse<LieferantModel[]>> {
    return this.request({
      method: 'get',
      url: '/lieferant',
    });
  }
}

const lieferantService = new LieferantService();
export default lieferantService;
