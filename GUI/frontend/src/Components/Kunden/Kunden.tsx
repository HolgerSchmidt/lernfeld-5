import { Button, Col, DatePicker, Divider, Input, Row, Table } from 'antd';
import Modal from 'antd/lib/modal/Modal';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { KundeModel } from '../../models/KundeModel';
import kundeService from '../../services/KundeService';

export interface Customer {
  NACHNAME?: string;
  VORNAME?: string;
  GEBURTSDATUM?: string;
  STRASSE?: string;
  HAUSNR?: string;
  PLZ?: string;
  ORT?: string;
  TELEFON?: string;
  EMAIL?: string;
}

const Kunden = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [allCustomers, setAllCustomers] = useState<KundeModel[]>([]);
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);
  const [customer, setCustomer] = useState<Customer | undefined>(undefined);

  useEffect(() => {
    getAllCustomers();
  }, []);

  const getAllCustomers = async (): Promise<void> => {
    setIsLoading(true);
    const response = await kundeService.getAllCustomers();
    setAllCustomers(response.data);
    setIsLoading(false);
  };

  const tableColumns = [
    {
      title: 'Customer Id',
      dataIndex: 'CustomerId',
      key: 'CustomerId',
    },
    {
      title: 'Last Name',
      dataIndex: 'LastName',
      key: 'LastName',
    },
    {
      title: 'First Name',
      dataIndex: 'FirstName',
      key: 'FirstName',
    },
    {
      title: 'Date of Birth',
      dataIndex: 'DateOfBirth',
      key: 'DateOfBirth',
    },
    {
      title: 'Street',
      dataIndex: 'Street',
      key: 'Street',
    },
    {
      title: 'HouseNr',
      dataIndex: 'HouseNr',
      key: 'HouseNr',
    },
    {
      title: 'Post code',
      dataIndex: 'PostCode',
      key: 'PostCode',
    },
    {
      title: 'Place of residence',
      dataIndex: 'PlaceOfResidence',
      key: 'PlaceOfResidence',
    },
    {
      title: 'Telefon',
      dataIndex: 'Telefon',
      key: 'Telefon',
    },
    {
      title: 'E-Mail',
      dataIndex: 'EMail',
      key: 'EMail',
    },
  ];

  const renderTableData = () => {
    return allCustomers.map((customer) => {
      return {
        key: customer.KUNDENNR,
        CustomerId: customer.KUNDENNR,
        LastName: customer.NACHNAME,
        FirstName: customer.VORNAME,
        DateOfBirth: new Date(customer.GEBURTSDATUM).toLocaleDateString(),
        Street: customer.STRASSE,
        HouseNr: customer.HAUSNR,
        PostCode: customer.PLZ,
        PlaceOfResidence: customer.ORT,
        Telefon: customer.TELEFON,
        EMail: customer.EMAIL,
      };
    });
  };

  const setNewCustomer = (key: string, value: string): void => {
    setCustomer({
      ...customer,
      [key]: value,
    });
  };

  const createNewCustomer = async (costumer: Customer): Promise<void> => {
    await kundeService.createNewCustomer(costumer);
    setCustomer(undefined);
    setIsModalVisible(false);
    getAllCustomers();
  };

  const onChangeDatePicker = (date: any, dateString: any): void => {
    setNewCustomer('GEBURTSDATUM', dateString);
  };

  return (
    <div className="content">
      <Row style={{ padding: '2rem 2rem' }}>
        <Col span={24}>
          <Divider
            style={{
              color: 'white',
              borderRadius: '2px',
              fontSize: '2rem',
            }}
          >
            Customers
          </Divider>
          <Button
            style={{ marginBottom: '1rem', backgroundColor: 'white' }}
            type="link"
            onClick={() => setIsModalVisible(!isModalVisible)}
          >
            Create new Customer
          </Button>
          <div className="recipetable">
            <Table
              dataSource={renderTableData()}
              columns={tableColumns}
              loading={isLoading}
              pagination={false}
            />
          </div>
        </Col>
      </Row>
      <Modal
        title="Create new customer"
        visible={isModalVisible}
        onOk={() => createNewCustomer(customer as Customer)}
        onCancel={() => setIsModalVisible(!isModalVisible)}
        okButtonProps={{
          disabled:
            !customer?.EMAIL ||
            !customer.GEBURTSDATUM ||
            !customer.HAUSNR ||
            !customer.NACHNAME ||
            !customer.ORT ||
            !customer.PLZ ||
            !customer.STRASSE ||
            !customer.TELEFON ||
            !customer.VORNAME,
        }}
      >
        <Divider orientation="left">Personal information</Divider>
        <Input
          style={{ marginBottom: '1rem' }}
          placeholder="Firstname"
          onChange={(e) => setNewCustomer('VORNAME', e.target.value)}
        ></Input>
        <Input
          placeholder="Lastname"
          onChange={(e) => setNewCustomer('NACHNAME', e.target.value)}
        ></Input>
        <DatePicker
          onChange={onChangeDatePicker}
          style={{ marginTop: '1rem', width: '100%' }}
          placeholder="Date of birth"
          defaultPickerValue={moment('1990/01/01', 'DD/MM/YYYY')}
        />
        <Divider orientation="left">Place of residence</Divider>
        <Input
          placeholder="Street"
          style={{ marginBottom: '1rem' }}
          onChange={(e) => setNewCustomer('STRASSE', e.target.value)}
        ></Input>
        <Input
          placeholder="House number"
          style={{ marginBottom: '1rem' }}
          onChange={(e) => setNewCustomer('HAUSNR', e.target.value)}
        ></Input>
        <Input
          placeholder="Post code"
          style={{ marginBottom: '1rem' }}
          onChange={(e) => setNewCustomer('PLZ', e.target.value)}
        ></Input>
        <Input
          placeholder="Place of residence"
          onChange={(e) => setNewCustomer('ORT', e.target.value)}
        ></Input>
        <Divider orientation="left">Kontaktmöglichkeiten</Divider>
        <Input
          placeholder="Telefon"
          style={{ marginBottom: '1rem' }}
          onChange={(e) => setNewCustomer('TELEFON', e.target.value)}
          type="tel"
        ></Input>
        <Input
          placeholder="E-Mail"
          onChange={(e) => setNewCustomer('EMAIL', e.target.value)}
          type="email"
        ></Input>
      </Modal>
    </div>
  );
};

export default Kunden;
