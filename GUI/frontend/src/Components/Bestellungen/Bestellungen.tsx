import { Button, Col, Divider, Input, Row, Select, Table } from 'antd';
import Modal from 'antd/lib/modal/Modal';
import React, { useEffect, useState } from 'react';
import { BestellungModel } from '../../models/BestellungModel';
import { KundeModel } from '../../models/KundeModel';
import { RezeptModel } from '../../models/RezeptModel';
import { ZutatModel } from '../../models/ZutatModel';
import bestellungService from '../../services/BestellungService';
import kundeService from '../../services/KundeService';
import rezeptService from '../../services/RezeptService';
import zutatService from '../../services/ZutatService';

interface RecipeOrderModel {
  key: number
  value: string
  recipes: RezeptModel
}

interface CustomerOrderModel {
  key: number
  value: string
  customer: KundeModel
}

const Orders = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [allOrders, setAllOrders] = useState<BestellungModel[]>([]);
  const [allRecipes, setAllRecipes] = useState<RezeptModel[] | undefined>(undefined)
  const [allCustomers, setAllCustomers] = useState<KundeModel[] | undefined>(undefined)
  const [allIngredients, setallIngredients] = useState<ZutatModel[] | undefined >(undefined)
  const [isNewOrderModalVisible, setIsNewOrderModalVisible] = useState<boolean>(false)
  const [customerOrder, setCustomerOrder] = useState<CustomerOrderModel | undefined>(undefined)
  const [recipeOrders, setrecipeOrders] = useState<RezeptModel[] | undefined>(undefined)
  const [ingredientOrders, setingredientOrders] = useState<ZutatModel[] | undefined>(undefined)
  const [invoice, setInvoice] = useState<string | undefined>(undefined)

  useEffect(() => {
    getAllOrders()
    getAllRecipes();
    getAllCustomers();
    getAllIngredients();
  }, []);

useEffect(() => {
    calculateInvoice()
  }, [recipeOrders, ingredientOrders]) 

  const getAllOrders = async (): Promise<void> => {
    setIsLoading(true);
    const response = await bestellungService.getAllRecipes();
    setAllOrders(response.data);
    setIsLoading(false);
  };

  const getAllRecipes = async (): Promise<void> => {
    setIsLoading(true)
    const response = await rezeptService.getAllRecipes();
    setAllRecipes(response.data)
    setIsLoading(false)
  }

  const getAllCustomers = async (): Promise<void> => {
    setIsLoading(true)
    const response = await kundeService.getAllCustomers();
    setAllCustomers(response.data)
    setIsLoading(false)
  }

  const getAllIngredients = async (): Promise<void> => {
    setIsLoading(true)
    const response = await zutatService.getAllIngredients();
    setallIngredients(response.data)
    setIsLoading(false)
  }

  const renderSelectRecipeOptions = () => {
    return allRecipes?.map((recipe) => {
      return {
        value: recipe.TITEL,
        text: recipe.TITEL,
        key: recipe.ID,
        recipe: recipe
      }
    })
  }

  const renderSelectCustomerOptions = () => {
    return allCustomers?.map((customer) => {
      return {
        value: `${customer.NACHNAME}, ${customer.VORNAME}`,
        text: customer.NACHNAME,
        key: customer.KUNDENNR,
        customer: customer
      }
    })
  }

  const renderSelectIngredientOptions = () => {
    return allIngredients?.map((ingredient) => {
      return {
        value: ingredient.BEZEICHNUNG,
        text: ingredient.BEZEICHNUNG,
        key: ingredient.ZUTATENNR,
        ingredient: ingredient
      }
    })
  }

 const calculateInvoice = () => {
  let totalAmount = 0

  const netPriceArray = recipeOrders?.map((recipe) => recipe.ZUTATEN.map((ingredient) => ingredient.NETTOPREIS))

  const netPrice = netPriceArray?.flatMap(x => x)

  netPrice?.forEach((element) => {
    totalAmount = totalAmount + parseFloat(element)
  })

  const netPriceIngredient = ingredientOrders?.map((ingredient) => ingredient.NETTOPREIS)
  netPriceIngredient?.forEach((element) => {
    totalAmount = totalAmount + parseFloat(element)
  })

  setInvoice(totalAmount.toFixed(2))

  }

  const setNewOrder = async () => {

    const mappedRecipes = recipeOrders?.map((recipe) => {
      return {
        ID: recipe.ID
      }
    })

    const mappedIngredients = ingredientOrders?.map((ingredient) => {
      return {
        ZUTATENNR: ingredient.ZUTATENNR
      }
    })
  

    const newRecipeOrderObject = {
      BESTELLDATUM: new Date(),
      RECHNUNGSBETRAG: invoice?.toString(),
      KUNDE: {
        KUNDENNR: customerOrder?.key
      },
      REZEPT: [...mappedRecipes as any],
      ZUTAT: [...mappedIngredients as any]
    }

    setIsLoading(true)
    await bestellungService.setNewRecipeOrder(newRecipeOrderObject)
    setIsLoading(false)
    await getAllOrders()
    setIsNewOrderModalVisible(false)
  }

  const renderTableData = () => {
    return allOrders.map((order) => {
      return {
        key: order.BESTELLNR,
        ID: order.BESTELLNR,
        CustomerId: order.KUNDE.KUNDENNR,
        LastName: order.KUNDE.NACHNAME,
        FirstName: order.KUNDE.VORNAME,
        OrderDate: new Date(order.BESTELLDATUM).toLocaleDateString(),
        InvoiceAmount: `${order.RECHNUNGSBETRAG}€`,
        OrderedIngredients: order.ZUTAT.map((x) => x.BEZEICHNUNG).join(', '),
        OrderedRecipes: order.REZEPT.map((x) => x.TITEL).join(', '),
      };
    });
  };

  const getCustomerLastNameFilters = () => {
    return allCustomers?.map((customer) => {
       return {
         text: customer.NACHNAME,
         value: customer.NACHNAME
       }
     })
   }

  const tableColumns = [
    {
      title: 'OrderNr',
      dataIndex: 'ID',
      key: 'ID',
      width: '2rem',
    },
    {
      title: 'CustomerId',
      dataIndex: 'CustomerId',
      key: 'CustomerId',
    },
    {
      title: 'Last Name',
      dataIndex: 'LastName',
      key: 'Last name',
      filters: getCustomerLastNameFilters(),
      onFilter: (value: any, record: any) => record.LastName.indexOf(value) === 0

    },
    {
      title: 'First Name',
      dataIndex: 'FirstName',
      key: 'FirstName',
    },
    {
      title: 'Date of Order',
      dataIndex: 'OrderDate',
      key: 'OrderDate',
    },
    {
      title: 'Ordered Ingredient(s)',
      dataIndex: 'OrderedIngredients',
      key: 'OrderedIngredients',
    },
    {
      title: 'Ordered Recipe(s)',
      dataIndex: 'OrderedRecipes',
      key: 'OrderedRecipes',
    },
    {
      title: 'Invoice amount',
      dataIndex: 'InvoiceAmount',
      key: 'InvoiceAmount',
    },
  ];



  return (
    <div className="content">
      <Row style={{ padding: '2rem 2rem' }}>
        <Col span={24}>
          <Divider
            style={{
              color: 'white',
              borderRadius: '2px',
              fontSize: '2rem',
            }}
          >
            Orders
          </Divider>
          <Button
            type="link"
            style={{ marginBottom: '1rem', backgroundColor: 'white'}}
            onClick={() => setIsNewOrderModalVisible(!isNewOrderModalVisible)}
          >
            New Order
          </Button>
          <div className="recipetable">
            <Table
              dataSource={renderTableData()}
              columns={tableColumns}
              loading={isLoading}
              pagination={false}
              onRow={(record) => {
                return {
                  onDoubleClick: () => console.log(record)
                };
              }}
            />
          </div>
        </Col>
      </Row>
      <Modal
        title="Create new order"
        visible={isNewOrderModalVisible}
        onCancel={() => setIsNewOrderModalVisible(!isNewOrderModalVisible)}
        onOk={setNewOrder}        
        > 
        <Select options={renderSelectCustomerOptions()} style={{width: "100%", marginBottom: "1rem"}} placeholder="Chose a customer" onChange={(key, customer: any) => setCustomerOrder(customer)}/>
        <Select options={renderSelectRecipeOptions()} style={{width: "100%", marginBottom: "1rem"}} mode="multiple" placeholder="Chose a Recipe - multiple choice available" onChange={(value: any, recipe) => setrecipeOrders(recipe.map((x: any) => x.recipe))}/> 
        <Select options={renderSelectIngredientOptions()} style={{width: "100%", marginBottom: "1rem"}} mode="multiple" placeholder="Chose an ingredient - multiple choice available"onChange={(value: any, ingredient) => setingredientOrders(ingredient.map((x: any) => x.ingredient))}/>
        <Input placeholder="Invoice amount" suffix="€" disabled value={invoice} />
      </Modal>

      
    </div>
  );
};

export default Orders;
