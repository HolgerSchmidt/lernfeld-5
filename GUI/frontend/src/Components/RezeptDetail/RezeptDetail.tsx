import { Button, Descriptions, Divider, Steps } from 'antd';
import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom';
import { RezeptModel } from '../../models/RezeptModel';
import { ZutatModel } from '../../models/ZutatModel';
import rezeptService from '../../services/RezeptService';

const RezeptDetail = (props: any) => {
  const [recipe, setRecipe] = useState<RezeptModel | undefined>(undefined)
  const [currentStep, setCurrentStep] = useState<number>(0)

  useEffect(() => {
    getRecipeById(props.match.params.id)
  }, [])

  let history = useHistory()

  const { Step } = Steps;

  const getRecipeById = async (id: number): Promise<void> => {
    const response = await rezeptService.getRecipeById(props.match.params.id)
    setRecipe(response.data)
  }

  const calculateCalories = (ingredients: ZutatModel[] | undefined): number => {
    let calories = 0
    if(ingredients) { 
    for (let index = 0; index < ingredients.length; index++) {
      calories = calories + ingredients[index].KALORIEN
    }}

    return calories
  }

  const calculateProteins = (ingredients: ZutatModel[] | undefined): number => {
    let proteins: number = 0
    if(ingredients) { 
      
    for (let index = 0; index < ingredients.length; index++) {
      proteins = proteins + parseFloat(ingredients[index].PROTEIN)
    }}

    return proteins
  }
 
  const calculatecarbohydrates = (ingredients: ZutatModel[] | undefined): number => {
    let carbohydrates = 0
    if(ingredients) { 
    for (let index = 0; index < ingredients.length; index++) {
      carbohydrates = carbohydrates + parseFloat(ingredients[index].KOHLENHYDRATE)
    }}

    return carbohydrates
  }

  const setRecipeStep = (step: number): void => {
    setCurrentStep(step)
  }

  return (
    <div className="content">
      <Button type="dashed" onClick={() => history.push("/")} style={{float: "left", marginLeft: '2rem', marginTop: '1rem'}}>Zurück</Button>
      <div className="test" style={{margin: '2rem'}}>
      <Divider
      orientation="left"
            style={{
              color: 'white',
              borderRadius: '2px',
              fontSize: '1.5rem',
              paddingTop: '1rem'
            }}
          >
            {recipe?.TITEL}
          </Divider>
    <Descriptions bordered style={{backgroundColor: 'white'}}>
      <Descriptions.Item label="Rezeptname"span={3}><b><u>{recipe?.TITEL}</u></b></Descriptions.Item>
      <Descriptions.Item label="Zutatenanzahl" span={1}>{recipe?.ZUTATEN.length}</Descriptions.Item>
      <Descriptions.Item label="Ernährungskategorie" span={2}>{recipe?.KATEGORIEN.map((category) => category.TITEL)}</Descriptions.Item>
      <Descriptions.Item label="Kalorien gesamt" span={1}>{calculateCalories(recipe?.ZUTATEN)}</Descriptions.Item>
      <Descriptions.Item label="Protein gesamt" span={1}>{calculateProteins(recipe?.ZUTATEN)}</Descriptions.Item>
      <Descriptions.Item label="Kohlenhydrate gesamt" span={1}>{calculatecarbohydrates(recipe?.ZUTATEN)}</Descriptions.Item>
      <Descriptions.Item label="Zubereitung">
      <Steps direction="vertical" current={currentStep} onChange={(e: number) => setRecipeStep(e)} >
          <Step title="Schritt 1" description="Lorem ipsum dolor sit amet consectetur, adipisicing elit. Id beatae voluptate, quaerat laboriosam impedit
          quam eos praesentium ex laborum veniam, natus labore repellat inventore ad eveniet libero! Vel, accusantium officiis!"
          />
          <Step title="Schritt 2" description="
          Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
          voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
          Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
          At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor
          sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et
          accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
          Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan
          et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer
          adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.   
          Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor
          in hendrerit in vulputate velit esse"
          />
          <Step title="Schritt 3" description="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore
          magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est
          Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
          erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
          amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
          At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.   
          Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero" 
          />
        </Steps>
      </Descriptions.Item>
  </Descriptions>
  <Divider orientation="left"></Divider>
    </div>
    </div>
  )
}

export default RezeptDetail;