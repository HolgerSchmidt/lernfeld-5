export interface LieferantModel {
  LIEFERANTENNR: number;
  LIEFERANTENNAME: string;
  STRASSE: string;
  HAUSNR: string;
  PLZ: string;
  ORT: string;
  TELEFON: string;
  EMAIL: string;
}
