import { AllergieModel } from './AllergieModel';
import { KategorieModel } from './KategorieModel';
import { ZutatModel } from './ZutatModel';

export interface RezeptModel {
  ID: number;
  TITEL: string;
  KATEGORIEN: KategorieModel[];
  ALLERGIEN: AllergieModel[];
  ZUTATEN: ZutatModel[];
}
