import { KundeModel } from '../models/KundeModel';
import { RezeptModel } from './RezeptModel';
import { ZutatModel } from './ZutatModel';

export interface BestellungModel {
  BESTELLNR: number;
  KUNDE: KundeModel;
  BESTELLDATUM: string;
  RECHNUNGSBETRAG: number;
  ZUTAT: ZutatModel[];
  REZEPT: RezeptModel[];
}
