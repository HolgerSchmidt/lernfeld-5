import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { RecipeEntity } from './recipes.entity';
import { RecipeRepository } from './recipes.repository';

@Controller('api/rezept')
export class RecipesController {
  constructor(
    @InjectRepository(RecipeRepository)
    private readonly repository: RecipeRepository,
  ) {}

  @Get()
  getAllRecipes() {
    return this.repository.getAllRecipes();
  }

  @Get("/:id")
  getRecipeById(@Param("id") id: number): Promise<RecipeEntity> {
    return this.repository.getRecipeById(id)
  }

  @Post()
  createNewRecipe(@Body() recipe: any) {
    return this.repository.createNewRecipe(recipe);
  }
}
