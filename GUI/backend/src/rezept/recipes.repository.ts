import { EntityRepository, Repository } from 'typeorm';
import { RecipeEntity } from './recipes.entity';

@EntityRepository(RecipeEntity)
export class RecipeRepository extends Repository<RecipeEntity> {
  getAllRecipes = async () => {
    return await this.find({
      relations: ['ZUTATEN', 'KATEGORIEN', 'ALLERGIEN'],
    });
  };

  getRecipeById = async (id: number): Promise<RecipeEntity> => {
    return await this.findOne(id, { relations: ['ZUTATEN', 'KATEGORIEN', 'ALLERGIEN']})
  }

  createNewRecipe = async (recipe: any) => {
    return await this.save(recipe);

  };
}
