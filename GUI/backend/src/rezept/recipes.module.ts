import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm/dist/typeorm.module';
import { RecipesController } from './recipes.controller';
import { RecipeEntity } from './recipes.entity';
import { RecipeRepository } from './recipes.repository';

@Module({
  imports: [TypeOrmModule.forFeature([RecipeEntity, RecipeRepository])],
  controllers: [RecipesController],
})
export class RecipesModule {}
