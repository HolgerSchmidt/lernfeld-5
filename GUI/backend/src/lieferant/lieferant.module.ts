import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LieferantController } from './lieferant.controller';
import { LieferantEntity } from './lieferant.entity';
import { LieferantRepository } from './lieferant.repository';

@Module({
  imports: [TypeOrmModule.forFeature([LieferantEntity, LieferantRepository])],
  controllers: [LieferantController],
})
export class LieferantModule {}
