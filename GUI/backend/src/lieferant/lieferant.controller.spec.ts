import { Test, TestingModule } from '@nestjs/testing';
import { LieferantController } from './lieferant.controller';

describe('LieferantController', () => {
  let controller: LieferantController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LieferantController],
    }).compile();

    controller = module.get<LieferantController>(LieferantController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
