import { Test, TestingModule } from '@nestjs/testing';
import { KundeController } from './kunde.controller';

describe('KundeController', () => {
  let controller: KundeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [KundeController],
    }).compile();

    controller = module.get<KundeController>(KundeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
