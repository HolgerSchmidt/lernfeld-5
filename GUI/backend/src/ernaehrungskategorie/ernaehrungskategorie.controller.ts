import { Controller, Get } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ErnaehrungskategorieRepository } from './ernaehrungskategorie.repository';

@Controller('api/ernaehrungskategorie')
export class ErnaehrungskategorieController {
  constructor(
    @InjectRepository(ErnaehrungskategorieRepository)
    private readonly repository: ErnaehrungskategorieRepository,
  ) {}

  @Get()
  findAllCategories() {
    return this.repository.findAllCategories();
  }
}
