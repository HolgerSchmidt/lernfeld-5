import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ErnaehrungskategorieController } from './ernaehrungskategorie.controller';
import { ErnaehrungskategorieEntity } from './ernaehrungskategorie.entity';
import { ErnaehrungskategorieRepository } from './ernaehrungskategorie.repository';

@Module({
  imports: [TypeOrmModule.forFeature([ErnaehrungskategorieEntity, ErnaehrungskategorieRepository])],
  controllers: [ErnaehrungskategorieController],
})
export class ErnaehrungskategorieModule {}
