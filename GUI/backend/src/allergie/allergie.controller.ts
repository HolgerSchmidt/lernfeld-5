import { Controller, Get } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AllergieRepository } from './allergie.repository';

@Controller('api/allergie')
export class AllergieController {
  constructor(
    @InjectRepository(AllergieRepository)
    private readonly repository: AllergieRepository,
  ) {}

  @Get()
  findAllAllergies() {
    return this.repository.findAllAllergies();
  }
}
