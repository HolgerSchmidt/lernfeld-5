import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AllergieController } from './allergie.controller';
import { AllergieEntity } from './allergie.entity';
import { AllergieRepository } from './allergie.repository';

@Module({
  imports: [TypeOrmModule.forFeature([AllergieEntity, AllergieRepository])],
  controllers: [AllergieController],
})
export class AllergieModule {}
