import { Test, TestingModule } from '@nestjs/testing';
import { AllergieController } from './allergie.controller';

describe('AllergieController', () => {
  let controller: AllergieController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AllergieController],
    }).compile();

    controller = module.get<AllergieController>(AllergieController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
