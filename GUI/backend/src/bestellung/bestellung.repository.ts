import { EntityRepository, Repository } from 'typeorm';
import { BestellungEntity } from './bestellung.entity';

@EntityRepository(BestellungEntity)
export class BestellungRepository extends Repository<BestellungEntity> {
  findAllOrders = async () => {
    return this.find({relations: ["KUNDE", "REZEPT", "ZUTAT"]});
  };

  setNewOrder = async (order: any) => {
    this.save(order)
  }
}
