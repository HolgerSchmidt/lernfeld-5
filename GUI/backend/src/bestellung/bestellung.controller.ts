import { Body, Controller, Get, Post } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BestellungEntity } from './bestellung.entity';
import { BestellungRepository } from './bestellung.repository';

@Controller('api/bestellung')
export class BestellungController {
  constructor(
    @InjectRepository(BestellungRepository)
    private readonly repository: BestellungRepository,
  ) {}

  @Get()
  findAllOrders() {
    return this.repository.findAllOrders();
  }

  @Post()
  setNewOrder(@Body() order: any) {
    return this.repository.setNewOrder(order)
  }
}
