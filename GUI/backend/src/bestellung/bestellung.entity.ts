import { KundeEntity } from 'src/kunde/kunde.entity';
import { RecipeEntity } from 'src/rezept/recipes.entity';
import { ZutatEntity } from 'src/zutat/zutat.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
  ManyToMany,
  JoinTable,
} from 'typeorm';

@Entity({ name: 'bestellung' })
export class BestellungEntity {
  @PrimaryGeneratedColumn()
  BESTELLNR: number;

  @ManyToOne(
    type => KundeEntity,
    kunde => kunde.BESTELLUNGEN,
  )
  @JoinColumn({
    name: 'KUNDENNR',
    referencedColumnName: 'KUNDENNR',
  })
  KUNDE: KundeEntity;

  @Column()
  BESTELLDATUM: string;

  @Column()
  RECHNUNGSBETRAG: number;

  @ManyToMany(type => RecipeEntity)
  @JoinTable({
    name: 'BESTELLUNGREZEPT',
    joinColumn: { name: 'BESTELLID', referencedColumnName: 'BESTELLNR' },
    inverseJoinColumn: { name: 'REZEPTID', referencedColumnName: 'ID' },
  })
  REZEPT: RecipeEntity[];

  @ManyToMany(type => ZutatEntity)
  @JoinTable({
    name: 'BESTELLUNGZUTAT',
    joinColumn: { name: 'BESTELLNR', referencedColumnName: 'BESTELLNR' },
    inverseJoinColumn: { name: 'ZUTATENNR', referencedColumnName: 'ZUTATENNR' },
  })
  ZUTAT: ZutatEntity[];
}
