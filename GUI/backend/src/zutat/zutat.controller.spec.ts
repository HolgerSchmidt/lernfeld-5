import { Test, TestingModule } from '@nestjs/testing';
import { ZutatController } from './zutat.controller';

describe('ZutatController', () => {
  let controller: ZutatController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ZutatController],
    }).compile();

    controller = module.get<ZutatController>(ZutatController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
