import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm/dist';
import { ZutatController } from './zutat.controller';
import { ZutatEntity } from './zutat.entity';
import { ZutatRepository } from './zutat.repository';

@Module({
  imports: [TypeOrmModule.forFeature([ZutatEntity, ZutatRepository])],
  controllers: [ZutatController],
})
export class ZutatModule {}
