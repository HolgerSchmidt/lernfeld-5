import { setNewIngredientDto } from 'src/dtos/setNewIngredientDto';
import { EntityRepository, Repository } from 'typeorm';
import { ZutatEntity } from './zutat.entity';

@EntityRepository(ZutatEntity)
export class ZutatRepository extends Repository<ZutatEntity> {
  findAllIngredients = async () => {
    return this.find({ relations: ['BESTELLUNG', 'LIEFERANT'] });
  };

  setNewIngredient = (
    ingredient: setNewIngredientDto,
  ): Promise<ZutatEntity> => {
    const newIngredient = {
      BEZEICHNUNG: ingredient.Bezeichnung,
      EINHEIT: ingredient.Einheit,
      NETTOPREIS: ingredient.Nettopreis,
      BESTAND: ingredient.Bestand,
      KALORIEN: ingredient.Kalorien,
      KOHLENHYDRATE: ingredient.Kohlenhydrate,
      PROTEIN: ingredient.Protein,
      LIEFERANT: {
        LIEFERANTENNR: ingredient.Lieferant,
      },
    };
    return this.save(newIngredient);
  };
}
