USE krautundrueben;

SELECT zutat.BEZEICHNUNG, rezept.TITEL FROM rezept
LEFT JOIN rezeptzutat ON rezept.ID = rezeptzutat.REZEPTID
RIGHT       JOIN zutat ON zutat.ZUTATENNR = rezeptzutat.ZUTATID
WHERE rezept.TITEL IS NOT null
ORDER BY rezept.titel