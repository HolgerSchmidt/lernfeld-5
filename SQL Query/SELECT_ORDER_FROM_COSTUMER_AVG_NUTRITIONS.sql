USE krautundrueben;

SELECT kunde.KUNDENNR, AVG(zutat.KALORIEN) AS 'AVG Kalorien', AVG(zutat.KOHLENHYDRATE) AS 'AVG Kohlenhydrate', AVG(zutat.PROTEIN) AS 'AVG PROTEIN' FROM bestellungrezept
INNER JOIN rezeptzutat ON bestellungrezept.REZEPTID = rezeptzutat.REZEPTID
INNER JOIN zutat ON rezeptzutat.ZUTATID = zutat.ZUTATENNR
INNER JOIN bestellung ON bestellungrezept.BESTELLID = bestellung.BESTELLNR
INNER JOIN kunde ON bestellung.KUNDENNR = kunde.KUNDENNR
GROUP BY kunde.KUNDENNR


