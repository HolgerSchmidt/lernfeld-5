USE krautundrueben;

SELECT rezept.titel AS 'REZEPTNAME', ernaehrungskategorie.TITEL AS 'ERNÄHRUNGSKATEGORIE' FROM rezept
LEFT JOIN rezepternaehrungskategorie
ON rezept.ID = rezepternaehrungskategorie.rezeptid
left JOIN ernaehrungskategorie
ON ernaehrungskategorie.ID = rezepternaehrungskategorie.ERNAEHRUNGSKATEGORIEID
ORDER BY rezept.TITEL
