USE krautundrueben;

CREATE TABLE rezeptallergie(
rezeptid INT,
allergieid INT,
PRIMARY KEY(rezeptid, allergieid));

ALTER TABLE rezeptallergie
ADD FOREIGN KEY (rezeptid) REFERENCES rezept(ID);

ALTER TABLE rezeptallergie
ADD FOREIGN KEY (allergieid) REFERENCES allergie(ID);