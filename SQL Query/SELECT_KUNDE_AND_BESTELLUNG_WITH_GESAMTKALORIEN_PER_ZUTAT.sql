USE krautundrueben;

SELECT kunde.NACHNAME, kunde.VORNAME, zutat.BEZEICHNUNG, bestellungzutat.MENGE, (zutat.KALORIEN * bestellungzutat.MENGE) AS gesamtkalorien
FROM kunde
JOIN bestellung ON kunde.KUNDENNR = bestellung.KUNDENNR
JOIN bestellungzutat ON bestellung.BESTELLNR = bestellungzutat.BESTELLNR
JOIN zutat ON bestellungzutat.ZUTATENNR = zutat.ZUTATENNR
ORDER BY bestellung.BESTELLNR